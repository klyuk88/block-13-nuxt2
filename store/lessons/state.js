export default () => ({
    courses: null,
    lessons: null,
    videoKey: null,
    course: {},
    error: {},
    telegram: {}
})